section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.iterate:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .iterate
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi    ; pointer to string
    mov rdx, rax    ; string length
    mov rax, 1      ; 'write' syscall identifier
    mov rdi, 1      ; stdout descriptor
    syscall
    xor rax, rax
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1 
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rsp
    mov rax, rdi
.add_null_terminator:
	dec rsp
    mov byte [rsp], 0
.add_digit:
    xor rdx, rdx
    mov r9, 10
	div r9
	or dl, '0'
    dec rsp
	mov [rsp], dl
	test rax, rax
    jne .add_digit
.print_result:
    mov rdi, rsp
    push r8
    call print_string
    pop r8
    mov rsp, r8
    xor rax, rax
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    test rdi, rdi
    jns .print_result
.print_minus:
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.print_result:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
.next_char:
    mov r8b, [rdi+rcx]
    mov r9b, [rsi+rcx]
    cmp r8b, r9b
    jne .different
    test r8b, r8b
    jz .equals
    inc rcx
    jmp .next_char
.different:
    mov rax, 0
    ret
.equals:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0  ; syscall number
    mov rdi, 0  ; stdin identifier
    mov rdx, 1  ; bytes to read
    lea rsi, [rsp-1]
    syscall
    test rax, rax
    jz .end_of_stream
    mov al, [rsp-1]
    ret
.end_of_stream:
    xor rax, rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi ; string pointer
    mov r9, rsi ; buffer length
    mov r10, 0  ; string length
.next_char:
    push r8
    push r9
    push r10
    call read_char
    pop r10
    pop r9
    pop r8
    cmp rax, 0x0
    je .success
    cmp rax, 0x20
    je .space_char
    cmp rax, 0x9
    je .space_char
    cmp rax, 0xA
    je .space_char
.letter_char:
    inc r10
    cmp r10, r9
    je .error
    mov [r8+r10-1], al
    jmp .next_char
.space_char:
    test r10, r10
    jz .next_char
.success:
    mov byte [r8+r10], 0x0
    mov rax, r8
    mov rdx, r10
    ret
.error:
    xor rax, rax
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    xor rcx, rcx
    mov r10, 10
.next_char:
	mov sil, [rdi + rcx]
	cmp sil, '0'
    jl .return
    cmp sil, '9'
    jg .return
    sub sil, '0'
    mul r10
    add rax, rsi
    inc rcx
	jmp .next_char
.return:
	mov rdx, rcx
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    jne .positive
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret
.positive:
    call parse_uint
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdx
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi
    pop rdx
    inc rax                ; add 0-terminator
    cmp rax, rdx
    jg .buffer_overflow
.iterate:
    mov r10, [rdi]
    mov [rsi], r10
    cmp byte [rsi], 0
    je .end
    inc rdi
    inc rsi
    jmp .iterate
.end:
    ret
.buffer_overflow:
    mov rax, 0
    ret
